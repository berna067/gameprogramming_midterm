﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleLogic : MonoBehaviour {

    // Use this for initialization
    private Rigidbody rb;
    private Vector3 initPos;
    private Vector3 movement;

    public int initVeloc;
    public int magnitude;
    public string direction;
    public int startDelay;//secs still obstacle starts moving

	void Start () {

        initPos = transform.position;
        intializeVector(direction);
        


        //Vector3 movement = new Vector3(0, 0.0f, -5);
        

        transform.Translate(movement * initVeloc * Time.deltaTime);
    }
	
	// Update is called once per frame
	void Update () {
        //Vector3 movement = new Vector3(0, 0.0f, -5);
        if (Time.timeSinceLevelLoad >= startDelay)
            transform.Translate(movement* initVeloc * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SouthWall") || other.gameObject.CompareTag("NorthWall") || other.gameObject.CompareTag("EastWall")
            || other.gameObject.CompareTag("WestWall"))
        {
            transform.position = initPos;
        }

    }
    void intializeVector(string dir)
    {
        if (dir.Equals("North"))
        {
            movement = new Vector3(0, 0.0f, magnitude);
        }
        else if(dir.Equals("East"))
        {
            movement = new Vector3(magnitude, 0.0f, 0);
        }
        else if (dir.Equals("South"))
        {
            movement = new Vector3(0, 0.0f, -1 * magnitude);
        }
        else if (dir.Equals("West"))
        {
            movement = new Vector3(-1 * magnitude, 0.0f, 0);
        }

    }
}
