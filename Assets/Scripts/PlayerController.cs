﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private Rigidbody rb;
    private int scene = 1;
    private Vector3 movement;
    private bool hasStarted;
    private bool hasEnded;
    private int count;
    public static int PICKUP_COUNT = 9;
    
    public float speed;
    public int movementDelay;//secs till player can move
    public Text winLoseText;
    public Button restartButton;
    public Button quitButton;

    private void Start()
    {
        rb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
        Time.timeScale = 1;
        hasStarted = false;
        hasEnded = false;
        count = 0;

        SetMessage("Get Ready.");
        restartButton.gameObject.SetActive(false);
        quitButton.gameObject.SetActive(false);
    }
   
    private void FixedUpdate()
    {
       
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        
        movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        if (Time.timeSinceLevelLoad >= movementDelay)
            rb.AddForce(movement * speed);
        
    }
    private void Update()
    {
        if (Time.timeSinceLevelLoad >= movementDelay + 1 && !hasEnded)
        {
           
            SetMessage("");
        }
        else if (Time.timeSinceLevelLoad >= movementDelay && !hasStarted)
        {
            hasStarted = true;
            SetMessage("Start!");
        }
       

    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Obstacle"))
        {
            Time.timeScale = 0;
            SetMessage("You Lost...");
            hasEnded = true;

        }
        else if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            if(count == PICKUP_COUNT)
            {
                hasEnded = true;
                Time.timeScale = 0;
                SetMessage("You Won!");
            }
    
        }
        
        if(hasEnded)
        {

            restartButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);
        }

    }
 
    private void SetMessage(string s)   
    {
        winLoseText.text = s;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(scene);
    }
}

